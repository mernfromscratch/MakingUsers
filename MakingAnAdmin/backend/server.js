const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const dotenv = require('dotenv');
const passport = require('passport');
const { makeAdmin } = require('./helperFunctions');

dotenv.config();
const app = express();

// MongoDB
const mongoUrl = "mongodb+srv://totesfortutorials:totesfortutorials@cluster0.whqxa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
})
.then(() => {
    console.log('DB CONNECTED');
    makeAdmin("admin", "admin");
})
.catch(e => console.log(`DB CONNECTION ERROR: ${e}`));


// Middleware
app.use(cors({credentials: true, origin: process.env.FRONTENDURL}));
app.use(cookieParser());
app.use(express.json({limit: '20mb'}));
app.use(session({ secret: "mylittlesecretsentence" }));
app.use(passport.initialize());
app.use(passport.session());

// Routes
app.use("/user", require('./routes/users'));
app.use('/api', require('./routes/songs'));

const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Listening to cool bits on port ${port}`));