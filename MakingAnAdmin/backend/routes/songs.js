const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConfig = require('../passport');

const {
    getAllSongs,
    getSong,
    postSong,
    putSong,
    deleteSong,
    getAllAttributes
} = require('../controllers/songs');

const { admin } = require('../controllers/users');

router.get('/songs/all', getAllSongs);
router.get('/song/:_id', getSong);
router.post('/song/', passport.authenticate('jwt', {session: false}), postSong);
router.put('/song/:_id', passport.authenticate('jwt', {session: false}), putSong);
router.delete('/song/:_id', passport.authenticate('jwt', {session: false}), admin, deleteSong);

router.get('/songs/attributes', getAllAttributes);

module.exports = router;