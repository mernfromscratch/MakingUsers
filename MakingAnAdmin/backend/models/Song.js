const mongoose = require('mongoose');
const {
    artistOrBandNames, 
    genres 
} =  require('../constants');

const SongSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minLength: 1,
        unique: true
    },
    length: {
        type: Number,
        min: 1,
        max: 72000,
        required: true
    },
    genre: {
        type: String,
        enum: genres,
        default: "Unknown"
    },
    artists: [{
        type: String,
        enum: artistOrBandNames
    }],
    timesHeard: {
        type: Number,
        min: 0,
        default: 0 
    },
    exclusive: {
        type: Boolean,
        default: false
    }
});

SongSchema.pre("save", () => { console.log(this) });

module.exports = mongoose.model('Song', SongSchema);