exports.artistOrBandNames = ["Beathoven", "ACBC", "The Wheatles", "The Dead North", "Aloe Block", "Not known"];
exports.genres = ["Metal", "Country", "Pop", "Classic", "New Age", "Rock", "Electro", "Unknown"];

// displayName, valueName, type, min, max, options, adminOnly
exports.songAttributes = [
    {displayName: "Title", valueName: "title", type: "text", adminOnly: false, default: ""},
    {displayName: "Length", valueName: "length", type: "number", min: "1", max: "72000", adminOnly: false, default: "60"},
    {displayName: "Genre", valueName: "genre", type: "select", options: this.genres, multiple: false, adminOnly: true, default: "Unknown"},
    {displayName: "Artists", valueName: "artists", type: "select", options: this.artistOrBandNames, multiple: true, adminOnly: false, default: ["Not known"]},
    {displayName: "Times heard", valueName: "timesHeard", type: "number", adminOnly: true, default: 0},
    {displayName: "Exclusive", valueName: "exclusive", type: "checkbox", adminOnly: true, default: false},
];

exports.userAttributes = [
    {displayName: "Your password", valueName: "password", type: "password", defaultValue: "", onlyForMakingAdmin: true},
    {displayName: "User name", valueName: "userName", type: "text", defaultValue: "", onlyForRegister: true},
    {displayName: "Password", valueName: "password", type: "password", defaultValue: "", onlyForRegister: true},
    {displayName: "First name", valueName: "firstName", type: "text", defaultValue: ""},
    {displayName: "Last name", valueName: "lastName", type: "text", defaultValue: ""},
    {displayName: "Age", valueName: "age", type: "number", min: 18, defaultValue: 25},
];