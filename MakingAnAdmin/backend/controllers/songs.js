const Song = require('../models/Song');
const {
    songAttributes
} = require('../constants');

exports.getAllSongs = async (req, res) => {
    try {
        const songs = await Song.find({}).exec();
        res.json(songs);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getSong = async (req, res) => {
    try {
        const song = await Song.findById(req.params._id).exec();
        res.json(song);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.postSong = async (req, res) => {
    try {
        let newSong = { ...req.body };
        if (!(req.user.role === 'admin')) {
            for (const attribute of songAttributes) {
                if (attribute.adminOnly) { delete newSong[attribute.valueName] }
            };
        };
        const song = await new Song(newSong).save();
        res.json(song);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.putSong = async (req, res) => {
    try {
        let updatedSong = { ...req.body };
        const oldSong = await Song.findById(req.params._id).exec();
        if (!(req.user.role === 'admin')) {
            for (const attribute of songAttributes) {
                if (attribute.adminOnly && !(updatedSong[attribute.valueName] === oldSong[attribute.valueName])) { 
                    updatedSong[attribute.valueName] = oldSong[attribute.valueName] 
                };
            };
        };
        const song = await Song.findByIdAndUpdate(req.params._id, updatedSong, { new: true }).exec();
        res.json(song);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.deleteSong = async (req, res) => {
    try {
        const song = await Song.findByIdAndDelete(req.params._id).exec();
        res.json(song);            
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getAllAttributes = async (req, res) => {
    res.json(songAttributes);
}
