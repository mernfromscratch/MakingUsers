import { Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Forbidden from './Forbidden';

const AdminRoute = ({ children, ...rest }) => {
    const { authState } = useSelector((state) => ({ ...state }));

    return authState.admin ? (
        <Route {...rest} />
    ) : (
        <Forbidden />
    );
};

export default AdminRoute;