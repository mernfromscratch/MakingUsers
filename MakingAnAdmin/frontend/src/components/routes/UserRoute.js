import { Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Forbidden from './Forbidden';

const UserRoute = ({ children, ...rest }) => {
    const { authState } = useSelector((state) => ({ ...state }));

    return authState.authenticated ? (
        <Route {...rest} />
    ) : (
        <Forbidden />
    );
};

export default UserRoute;