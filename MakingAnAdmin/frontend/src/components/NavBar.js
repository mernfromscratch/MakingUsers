import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { logoutUserAction } from '../redux/actions/authActions';

import './NavBar.css';

const NavBar = () => {

    const history = useHistory();
    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);

    const logout = () => {
        dispatch(logoutUserAction());
        history.push("/");
    }

    return (
        <div className="navbar">
            <div className="navitem left-nav" onClick={() => history.push("/")}>
                Home
            </div>
            {authState.admin
                ?   <div className="navitem left-nav" onClick={() => history.push("/admin/makeAdmin")}>
                        Admin
                    </div>
                :   <></>        
            }
            {authState.authenticated 
                ?   <></>
                :   <div className="navitem right-nav" onClick={() => history.push("/user/register")}>
                        Register
                    </div>        
            }
            {authState.authenticated 
                ?   <div className="navitem right-nav" onClick={() => logout()}>
                        Log out
                    </div>
                :   <></>        
            }
            <div className="navitem right-nav" onClick={() => authState.authenticated ? history.push("/user/edit") : history.push("/user/login") }>
                { authState.authenticated ? "Profile" : "Login" }
            </div>
        </div>
    );
};

export default NavBar;