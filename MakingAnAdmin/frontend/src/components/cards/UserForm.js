import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
    registerUserAction,
    getCurrentUserAction,
    clearCurrentUserInfoAction,
    clearUserMessageAction,
    putUserAction,
    postAdminAction
} from '../../redux/actions/authActions';

import { getUserAttributes } from '../../helperFunctions/auth';

const UserForm = ({ history, location }) => {
    const [user, setUser] = useState({});
    const [password, setPassword] = useState("");
    const [attributes, setAttributes] = useState([]);
    const actionToDispatch = { edit: putUserAction, register: registerUserAction };
    const action = location.pathname.split('/').pop();

    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);

    useEffect(() => {
        if (authState.authenticated && (action === "edit")) { dispatch(getCurrentUserAction()) };
        getUserAttributes()
            .then((attributes) => {
                setAttributes(attributes);
                if (!(action === "edit")) {
                    let defaultUser = {};
                    attributes.map((attribute) => { defaultUser[attribute.valueName] = attribute.defaultValue });
                    setUser(defaultUser);
                };
            })
            .catch((error) => console.log(error));
    }, []);

    useEffect(() => {
        authState.authenticated && !(action === "makeAdmin") && setUser(authState.currentUserToEdit);                                           // If editing user, fetch user info
        authState.message && !(authState.message.errorOccurred) && !(authState.message.text === "") && dispatch( clearUserMessageAction() );    // Clear message from backend if info sent to backend successfully changed something
        authState.message && (action === "register") && !(authState.message.errorOccurred) && history.push("/user/login");                      // If registering was successful, go to login page
        authState.message && (action === "makeAdmin") && !(authState.message.errorOccurred) && history.push("/user/makeAdmin");                 // Reload page if a new admin was successfully created
        return () => {  }
    }, [authState]);

    const sendToBack = () => {
        !(action === "makeAdmin") 
            ?   dispatch(actionToDispatch[action](user)) 
            :   dispatch(postAdminAction(user, password));
        if (!(action === "edit")) { dispatch( clearCurrentUserInfoAction() ) }; 
    }

    const changeUser = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value });
    }

    const changePassword = (e) => {
        setPassword({ ...password, [e.target.name]: e.target.value });
    }

    const makeRow = (attribute) => {
        const { displayName, valueName, type, min, max, onlyForRegister, onlyForMakingAdmin } = attribute;
        if ((action === ("edit")) && onlyForRegister) { return <></> };
        if (!(action === ("makeAdmin")) && onlyForMakingAdmin) { return <></> };
        return (
            <tr key={onlyForMakingAdmin ? `${valueName}_making_admin` : valueName}>
                <td>
                    { displayName }
                </td>
                <td>
                     <input type={type} name={valueName} min={min} max={max} value={user[valueName]} onChange={(e) => onlyForMakingAdmin ? changePassword(e) : changeUser(e)}  />
                </td>
            </tr>
        );
    };

    return (
        <div>
            <div>
                {action === "edit" 
                    ?   <div onClick={() => history.push("/user/passwordChange")} className="changeButton">
                            Change password
                        </div>
                    : <></>
                }
            </div>
            <div className="vhCenter bordered">
                <table>
                    <tbody>
                        {user && attributes.map(attribute => makeRow(attribute)) }
                    </tbody>
                </table>
                <div className="changeButtonLight" onClick={sendToBack}>
                    Submit
                </div>
            </div>
        </div>
    )
};

export default UserForm;