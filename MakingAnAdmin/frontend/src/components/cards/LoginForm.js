import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginUserAction } from '../../redux/actions/authActions';

const LoginForm = ({ history }) => {

    const [loginCredentials, setLoginCredentials] = useState({ userName: "", password: "" });
    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);

    const changeCredentials = (e) => {
        setLoginCredentials({ ...loginCredentials, [e.target.name]: e.target.value });
    }

    const login = () => {
        dispatch(loginUserAction(loginCredentials));
    }

    useEffect(() => {
        authState.authenticated && history.push("/");
    }, [authState]);

    return (
        <div className="vhCenter bordered">
            <table>
                <tbody>
                    <tr>
                        <td>
                            Username
                        </td>
                        <td>
                            <input type="text" name="userName" value={loginCredentials.userName} onChange={changeCredentials} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password
                        </td>
                        <td>
                            <input type="password" name="password" value={loginCredentials.password} onChange={changeCredentials} />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div className="changeButtonLight" onClick={login}>
                Log In
            </div>
        </div>
    );
};

export default LoginForm;