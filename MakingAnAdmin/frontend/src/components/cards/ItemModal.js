import ItemForm from './ItemForm';


const ItemModal = ({admin, item, setItem, itemAttributes, sendToBack, setVisible}) => {

    return (
        <ItemForm 
            admin={admin} 
            item={item} 
            setItem={setItem} 
            itemAttributes={itemAttributes} 
            sendToBack={sendToBack} 
            cancel={setVisible} 
        />
    );
};

export default ItemModal;