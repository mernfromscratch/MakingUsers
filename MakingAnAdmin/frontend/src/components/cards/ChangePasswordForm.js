import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { putPasswordAction } from '../../redux/actions/authActions';

const ChangePasswordForm = ({ history }) => {

    const [passwordCredentials, setpasswordCredentials] = useState({ password: "", newPassword: "" });
    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);

    const changeCredentials = (e) => {
        setpasswordCredentials({ ...passwordCredentials, [e.target.name]: e.target.value });
    }

    const changePassword = () => {
        dispatch(putPasswordAction(passwordCredentials));
    }

    useEffect(() => {
        authState.message && (!authState.message.errorOccurred) && (authState.message.text === "Password successfully updated") && history.push("/");
    }, [authState]);

    return (
        <div className="vhCenter bordered">
            <table>
                <tbody>
                    <tr>
                        <td>
                            Old password
                        </td>
                        <td>
                            <input type="password" name="password" value={passwordCredentials.password} onChange={changeCredentials} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            New password
                        </td>
                        <td>
                            <input type="password" name="newPassword" value={passwordCredentials.newPassword} onChange={changeCredentials} />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div className="changeButtonLight" onClick={changePassword}>
                Change password
            </div>
        </div>
    );
};

export default ChangePasswordForm;