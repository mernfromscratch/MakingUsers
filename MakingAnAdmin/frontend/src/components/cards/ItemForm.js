const ItemForm = ({admin, item, setItem, itemAttributes, sendToBack, cancel}) => {

    const changeItem = (e) => {
        const value = e.target.type === "checkbox" ? !(item[e.target.name]) : e.target.value;
        setItem({ ...item, [e.target.name]: value });
    } 

    const changeSelectItemMultiple = (e) => {
        let newOption = e.target.value;
        let selectedOptions = item[e.target.name];
        if (selectedOptions.includes(newOption)) { selectedOptions = selectedOptions.filter(sOption => { return !(sOption === newOption) }) }
        else { selectedOptions.push(newOption) };
        setItem({ ...item, [e.target.name]: selectedOptions });
    };

    const giveInput = (input) => {
        const { displayName, valueName, type, min, max, options, multiple, adminOnly } = input;
        if (adminOnly && !admin) { return (<></>) };
        return (
            <tr key={valueName}>
                <td>
                    { displayName }
                </td>
                <td>
                    {type === 'select'
                        ?   (   <select key={valueName} name={valueName} multiple={multiple} onChange={(e) => multiple ? changeSelectItemMultiple(e) : changeItem(e)} value={item[valueName]}>
                                    {options.map((value) => (
                                        <option key={value} value={value}>
                                            {value}
                                        </option>
                                    ))}
                                </select> 
                            )
                        :   type === 'checkbox' 
                                ?   (
                                        <input key={valueName} name={valueName} type={type} checked={item[valueName]} defaultChecked={item[valueName]} min={min} max={max} onChange={changeItem} />
                                    )
                                :   (
                                        <input key={valueName} name={valueName} type={type} value={item[valueName]} min={min} max={max} onChange={changeItem} />
                                    )
                            
                    }
                </td>
            </tr>    
    )};

    return (
        <div className="modalContent">
            <div className="modalBody">
                <table>
                    <tbody>
                        {itemAttributes && itemAttributes.map(attribute => (
                            giveInput(attribute)
                        ))}
                    </tbody>
                </table>
            </div>
            <div className="modalFooter">
                <button className="changeButtonNothingFancy" onClick={sendToBack}>
                    Submit
                </button>
                <button className="changeButtonNothingFancy" onClick={cancel}>
                    Cancel
                </button>
            </div>
        </div>
    );
};

export default ItemForm;