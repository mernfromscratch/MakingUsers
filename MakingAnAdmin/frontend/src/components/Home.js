import { useSelector, useDispatch } from 'react-redux';
import { useState, useEffect } from 'react';
import { getAllSongsAction, putSongAction, postSongAction, deleteSongAction } from '../redux/actions/songActions';
import { getSong, getSongAttributes } from '../helperFunctions/song';

import ItemModal from './cards/ItemModal';

const Home = () => {

    const { songState, authState } = useSelector(state => state);
    const [song, setSong] = useState({});
    const [attributes, setAttributes] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getAllSongsAction());
        getSongAttributes()
            .then((allAttributes) => {
                setAttributes(allAttributes);
            })
            .then(() => {
                setSongDefault();
            })
            .catch(err => console.log(err));
    }, []);

    useEffect(() => {
        setModalVisible(false);
    }, [songState.songs]);

    const setSongDefault = () => {
        let songDefault = {};
        attributes.map((attribute) => { songDefault[attribute.valueName] = attribute.default });
        setSong(songDefault);
    }

    const setModalAndDefaultSong = () => {
        setModalVisible(!modalVisible);
        setSongDefault();
    }

    const setModalAndSong = (_id) => {
        setModalVisible(true);
        getSong(_id).then((returnedSong) => {setSong(returnedSong)}).catch(err => console.log(err));
    };

    const sendToBack = () => {
        song._id ? dispatch(putSongAction(song)) : dispatch(postSongAction(song));
    }

    const prettifyMultipleSelectEntries = (entries) => {
        let prettifiedEntries = "";
        entries.map((entry) => prettifiedEntries = prettifiedEntries.concat(` ${entry} &`));
        prettifiedEntries = prettifiedEntries.slice(0, prettifiedEntries.length - 2);
        return prettifiedEntries;
    }

    const makeRow = (item) => (
        <tr key={item.title}>
            {attributes.map(attribute => (
                <td key={attribute.valueName}>
                    {attribute.type === "checkbox" 
                        ?   (item[attribute.valueName] ? "Yes" : "No") 
                        :   attribute.multiple 
                                ?   prettifyMultipleSelectEntries(item[attribute.valueName])
                                :   item[attribute.valueName]
                    }
                </td>
            ))}
            <td>
                <button className="changeButtonNothingFancy" style={{display: authState.authenticated ? "inline" : "none", margin: "5px"}} onClick={() => setModalAndSong(item._id)}>
                    Edit
                </button>
                <button className="changeButtonNothingFancy" style={{ display: (authState.user.role === "admin" ? "inline" : "none") }} onClick={() => dispatch(deleteSongAction(item._id))}>
                    Delete
                </button>
            </td>
        </tr>
    );

    return (
        <div>
            <div className="modal" style={{ visibility: (modalVisible ? "visible" : "hidden" ) }}>
                <ItemModal 
                    admin={authState.user.role === "admin" ? true : false} 
                    item={song} 
                    setItem={setSong} 
                    itemAttributes={attributes} 
                    sendToBack={sendToBack} 
                    setVisible={() => setModalVisible(!modalVisible)} 
                />
            </div>
            <div style={{ visibility: (authState.authenticated ? "visible" : "hidden") }}>
                <div className="changeButton" onClick={setModalAndDefaultSong}>
                    Create Song
                </div>
            </div>
            <div className="centered">
                <table>
                    <thead>
                        <tr>
                            {attributes.map(attribute => (
                                <th key={attribute.valueName}>
                                    {attribute.displayName}
                                </th>
                            ))}
                            <th>
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {songState.songs.map(song => makeRow(song))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default Home;