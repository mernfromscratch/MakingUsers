const axios = require('axios');

export const register = async (user) => {
    const userRegistered = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/register`, user, {withCredentials: true});
    if (userRegistered.error) { return userRegistered.error };
    return userRegistered.data;        
};

export const login = async (userName, password) => {
    const userLogin = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/login`, {userName, password}, {withCredentials: true});        
    if (userLogin.error) { return userLogin.error };
    return userLogin.data;
};

export const logout = async () => {
    const userLogout = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/logout`, {}, {withCredentials: true});
    if (userLogout.error) { return userLogout.error };
    return userLogout.data;
};

export const isAuthenticated = async () => {
    const isAuthenticated = await axios.get(`${process.env.REACT_APP_BACKENDURL}/user/authenticated`, {withCredentials: true});
    if (isAuthenticated.error) { return isAuthenticated.error };
    return isAuthenticated.data;
}

export const getCurrentUser = async () => {
    const currentUser = await axios.get(`${process.env.REACT_APP_BACKENDURL}/user/getCurrent`, {withCredentials: true});
    if (currentUser.error) { return currentUser.error };
    return currentUser.data;
};

export const putUser = async (user) => {
    const userPut = await axios.put(`${process.env.REACT_APP_BACKENDURL}/user/edit`, user, {withCredentials: true});
    if (userPut.error) { return userPut.error };
    return userPut.data;        
};

export const putPassword = async (passwordOldAndNew) => {
    const passwordPut = await axios.put(`${process.env.REACT_APP_BACKENDURL}/user/changePassword`, passwordOldAndNew, {withCredentials: true});
    if (passwordPut.error) { return passwordPut.error };
    return passwordPut.data;        
};

export const postAdmin = async (newAdmin, password) => {
    const response = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/makeAdmin`, {newAdmin, password}, {withCredentials: true});
    if (response.error) { return response.error };
    return response.data;
};
