import axios from 'axios';

export const getAllSongs = async () => {
    const songs = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/songs/all`);
    if (songs.error) { return songs.error};
    return songs.data;
};

export const postSong = async (song) => {
    const songFromBack = await axios.post(`${process.env.REACT_APP_BACKENDURL}/api/song/`, song, {withCredentials: true});
    if (songFromBack.error) {return songFromBack.error};
    return songFromBack.data;
};

export const putSong = async (song) => {
    const songFromBack = await axios.put(`${process.env.REACT_APP_BACKENDURL}/api/song/${song._id}`, song, {withCredentials: true});
    if (songFromBack.error) { return songFromBack.error };
    return songFromBack.data;
};

export const deleteSong = async (_id) => {
    const songFromBack = await axios.delete(`${process.env.REACT_APP_BACKENDURL}/api/song/${_id}`, {withCredentials: true});
    if (songFromBack.error) { return songFromBack.error };
    return songFromBack.data;
};