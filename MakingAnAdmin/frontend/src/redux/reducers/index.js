import { combineReducers } from 'redux';
import authReducer from './authReducer';
import songReducer from './songReducer';

const rootReducer = combineReducers({
    authState: authReducer,
    songState: songReducer
})

export default rootReducer;