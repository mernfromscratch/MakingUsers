import {
    GET_ALL_SONGS, GET_ALL_SONGS_SUCCESS, GET_ALL_SONGS_ERROR,
    POST_SONG, POST_SONG_SUCCESS, POST_SONG_ERROR,
    PUT_SONG, PUT_SONG_SUCCESS, PUT_SONG_ERROR,
    DELETE_SONG, DELETE_SONG_SUCCESS, DELETE_SONG_ERROR
} from '../actions/songActionTypes';

import { sortSongs } from '../../helperFunctions/song';

const initialState = {
    songs: [],
    loading: true,
    error: null
};

export default function songReducer(state = initialState, action) {
    switch(action.type) {
        case GET_ALL_SONGS:
            return { ...state, loading: true };
        case GET_ALL_SONGS_SUCCESS:
            return { ...state, loading: false, songs: action.payload };
        case GET_ALL_SONGS_ERROR:
            return { ...state, loading: false, error: action.payload };

        case POST_SONG:
            return { ...state, loading: true };
        case POST_SONG_SUCCESS:
            let newSongs = state.songs.slice();
            newSongs.push(action.payload);
            return { ...state, loading: false, songs: newSongs.sort(sortSongs) };
        case POST_SONG_ERROR:
            return { ...state, loading: false, error: action.payload };
        
        case PUT_SONG:
            return { ...state, loading: true };
        case PUT_SONG_SUCCESS:
            let editedSongs = state.songs.filter(song => !(song._id === action.payload._id));
            editedSongs.push(action.payload);
            return { ...state, loading: false, songs: editedSongs.sort(sortSongs) };
        case PUT_SONG_ERROR:
            return { ...state, loading: false, error: action.payload };
        
        case DELETE_SONG:
            return { ...state, loading: true };
        case DELETE_SONG_SUCCESS:
            let deletedSongs = state.songs.filter(song => !(song._id === action.payload._id));
            return { ...state, loading: false, songs: deletedSongs.sort(sortSongs) };
        case DELETE_SONG_ERROR:
            return { ...state, loading: false, error: action.payload };
        
        default:
            return { ...state };
    };
};

