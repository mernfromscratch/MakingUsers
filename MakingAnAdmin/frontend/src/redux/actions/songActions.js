import {
    GET_ALL_SONGS, GET_ALL_SONGS_SUCCESS, GET_ALL_SONGS_ERROR,
    POST_SONG, POST_SONG_SUCCESS, POST_SONG_ERROR,
    PUT_SONG, PUT_SONG_SUCCESS, PUT_SONG_ERROR,
    DELETE_SONG, DELETE_SONG_SUCCESS, DELETE_SONG_ERROR
} from './songActionTypes';

import {
    getAllSongs,
    postSong,
    putSong,
    deleteSong
} from '../api/song';

export const getAllSongsAction = () => async dispatch => {
    dispatch({ type: GET_ALL_SONGS });
    getAllSongs()
        .then((songs) => {
            dispatch({
                type: GET_ALL_SONGS_SUCCESS,
                payload: songs 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_ALL_SONGS_ERROR,
                payload: error
            });
        });
};

export const postSongAction = (song) => async dispatch => {
    dispatch({ type: POST_SONG });
    postSong(song)
        .then((songFromBack) => {
            dispatch({
                type: POST_SONG_SUCCESS,
                payload: songFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: POST_SONG_ERROR,
                payload: error
            });
        });
};

export const putSongAction = (song) => async dispatch => {
    dispatch({ type: PUT_SONG });
    putSong(song)
        .then((songFromBack) => {
            dispatch({
                type: PUT_SONG_SUCCESS,
                payload: songFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: PUT_SONG_ERROR,
                payload: error
            });
        });
};

export const deleteSongAction = (_id) => async dispatch => {
    dispatch({ type: DELETE_SONG });
    deleteSong(_id)
        .then((songFromBack) => {
            dispatch({
                type: DELETE_SONG_SUCCESS,
                payload: songFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: DELETE_SONG_ERROR,
                payload: error
            });
        });
};