import axios from 'axios';

export const getSong = async (_id) => {
    try {
        const song = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/song/${_id}`);
        return song.data;
    } catch (error) {
        return {};
    }
};

export const getSongAttributes = async () => {
    try {
        const attributes = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/songs/attributes`);
        return attributes.data;
    } catch (error) {
        return [];
    }
};

export const sortSongs = (songA, songB) => {
    if (songA.title < songB.title) {return -1}
    else if (songA.title > songB.title) {return 1}
    else {return 0};
};