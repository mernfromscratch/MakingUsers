import axios from 'axios';

export const getUserAttributes = async () => {
    const attributes = await axios.get(`${process.env.REACT_APP_BACKENDURL}/user/attributes`);
    if (attributes.error) { return attributes.error };
    return attributes.data;
};