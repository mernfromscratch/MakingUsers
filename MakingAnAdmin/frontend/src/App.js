import './App.css';
import {Route, Switch} from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { checkIfAuthenticatedUserAction } from './redux/actions/authActions';

import UserRoute from './components/routes/UserRoute';
import AdminRoute from './components/routes/AdminRoute';
import Home from './components/Home';
import NavBar from './components/NavBar';
import UserForm from './components/cards/UserForm';
import LoginForm from './components/cards/LoginForm';
import ChangePasswordForm from './components/cards/ChangePasswordForm';
import { useState } from 'react';

function App() {

  const dispatch = useDispatch();

  useState(() => {
    dispatch(checkIfAuthenticatedUserAction());
  }, []);

  return (
    <div className="App">
      <NavBar />
      <div>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/user/register' component={UserForm} />
            <Route exact path='/user/login' component={LoginForm} />
            <UserRoute exact path='/user/passwordChange' component={ChangePasswordForm} />
            <UserRoute exact path='/user/edit' component={UserForm} />
            <AdminRoute exact path='/admin/makeAdmin' component={UserForm} />
          </Switch>
      </div>
    </div>
  );
};

export default App;
